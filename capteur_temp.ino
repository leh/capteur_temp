//libraries needed
#include <LiquidCrystal.h>
#include <DallasTemperature.h>
#include <OneWire.h>
//port sensor is plugged in to
#define ONE_WIRE_BUS 8 
// ports the LCD is using on Arduino !
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress tempDeviceAddress; 
float temp;
int numberOfDevices;

void setup() {
  Serial.begin(9600);// talking to ardiuno 
  sensors.begin(); //starts up the library
  int numberOfDevices = sensors.getDeviceCount(); // how many devices there are. 
  sensors.getAddress(tempDeviceAddress, 0);
  temp = sensors.getTempC(tempDeviceAddress);
  Serial.print(temp);
  lcd.begin(16, 2); // size of lcd screen. 16 across and 2 down.
  lcd.print("Temperature"); // what we see on the screen 
}

void loop() {
  sensors.requestTemperatures(); // requests temp.
  lcd.setCursor(0, 1);// where to print on the screen. O - top line, 1 is bottom line
  lcd.print(temp); // 
  lcd.print("C");
  delay(2000); 
}
